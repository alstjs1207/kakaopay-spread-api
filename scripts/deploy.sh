#! /bin/bash

REPOSITORY=/home/pay/step1
PROJECT_NAME=kakaopay-spread-api

cd $REPOSITORY/$PROJECT_NAME/

echo ">Git Pull"

git pull


echo "> project build start"

mvn package

echo "> Build file copy"
echo "> copy file location: ${REPOSITORY}"

cp target/*.jar $REPOSITORY/

echo "> now application pid check"

CURRENT_PID=$(pgrep -f ${PROJECT_NAME}.*.jar)

echo "> running application pid: $CURRENT_PID"

if [ -z "$CURRENT_PID" ]; then
	echo "> Not running application"
else
	echo "> kill -15 $CURRENT_PID"
	kill -15 $CURRENT_PID
	sleep 5
fi

echo "> new application deploy"

JAR_NAME=$(ls -tr $REPOSITORY/ | grep jar | tail -n 1)

echo "> JAR Name: $JAR_NAME"

nohup java -jar $REPOSITORY/$JAR_NAME 2>&1 &