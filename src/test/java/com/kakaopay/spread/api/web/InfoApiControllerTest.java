package com.kakaopay.spread.api.web;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.kakaopay.spread.api.web.dto.InfoRequestDto;
import com.kakaopay.spread.api.web.dto.InfoResponseDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class InfoApiControllerTest {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	
	@Test
	public void t1_info() {
		
		//given
		long user_id = 891207;
		String room_id = "mbc";
		String access_token = "1c3";
		
		InfoRequestDto requestDto = InfoRequestDto.builder()
												.access_token(access_token)
												.build();
		
		String url = "http://localhost:"+port+"/kpay/restv1/info";
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-USER-ID", String.valueOf(user_id));
		headers.add("X-ROOM-ID", room_id);
		
		HttpEntity<InfoRequestDto> requestEntity = new HttpEntity<InfoRequestDto>(requestDto,headers);
		
		//when
		ResponseEntity<InfoResponseDto> responseEntity = restTemplate.postForEntity(url, requestEntity, InfoResponseDto.class);
		
		//then
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(responseEntity.getBody().getAmount()).isGreaterThan(0L);
		assertThat(responseEntity.getBody().getInput_date()).isNotNull();
		
		if(responseEntity.getBody().getRevUserList().size() > 0) {
			responseEntity.getBody().getRevUserList().stream().forEach(x -> assertThat(x.getUser_id()).isNotNull());
		}
		
		//responseEntity.getBody().getRevUserList().stream().forEach(x -> log.info("id is {} amount is {}",x.getUser_id(),x.getAmount()));
		
	}

}
