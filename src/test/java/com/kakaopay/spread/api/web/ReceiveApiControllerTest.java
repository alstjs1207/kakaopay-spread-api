package com.kakaopay.spread.api.web;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.kakaopay.spread.api.web.dto.ReceiveRequestDto;
import com.kakaopay.spread.api.web.dto.ReceiveResponseDto;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ReceiveApiControllerTest {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	/**
	 * 받기 테스트
	 */
	@Test
	public void t1_receive() {
		
		//given
		long user_id = 911015;
		String room_id = "mbc";
		String access_token = "1c3";
		
		ReceiveRequestDto requestDto = ReceiveRequestDto.builder()
														.access_token(access_token)
														.build();
		
		String url = "http://localhost:"+port+"/kpay/restv1/receive";
		
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-USER-ID", String.valueOf(user_id));
		headers.add("X-ROOM-ID", room_id);
		
		HttpEntity<ReceiveRequestDto> requestEntity = new HttpEntity<ReceiveRequestDto>(requestDto,headers);
		
		//when
		ResponseEntity<ReceiveResponseDto> responseEntity = restTemplate.exchange(url,HttpMethod.PUT, requestEntity, ReceiveResponseDto.class);
		
		//then
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(responseEntity.getBody().getAmount()).isGreaterThan(0L);
		
	}

}
