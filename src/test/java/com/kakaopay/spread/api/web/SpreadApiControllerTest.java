package com.kakaopay.spread.api.web;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.kakaopay.spread.api.web.dto.SpreadsRequestDto;
import com.kakaopay.spread.api.web.dto.SpreadsResponseDto;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpreadApiControllerTest {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	/**
	 * 뿌리기 테스트
	 */
	@Test
	public void t1_spread() {
		
		//given
		long user_id = 891207;
		String room_id = "mbc";
		long amount = 1000;
		int people = 2;
		
		SpreadsRequestDto requestDto = SpreadsRequestDto.builder()
														.amount(amount)
														.people(people)
														.build();
		
		String url = "http://localhost:"+port+"/kpay/restv1/spread";
		
		// 헤더에 사용자 ID와 대화 방 ID 담기
		HttpHeaders headers = new HttpHeaders();
		headers.add("X-USER-ID", String.valueOf(user_id));
		headers.add("X-ROOM-ID", room_id);
		
		HttpEntity<SpreadsRequestDto> requestEntity = new HttpEntity<SpreadsRequestDto>(requestDto,headers);
		
		//when
		ResponseEntity<SpreadsResponseDto> responseEntity = restTemplate.postForEntity(url, requestEntity, SpreadsResponseDto.class);
		
		//then
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(responseEntity.getBody().getAccess_token()).isNotNull();
		
	}
	
}
