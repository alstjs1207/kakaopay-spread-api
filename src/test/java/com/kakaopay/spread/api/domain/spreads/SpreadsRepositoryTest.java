package com.kakaopay.spread.api.domain.spreads;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.LongStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.kakaopay.spread.api.web.dto.RevUserDto;

import lombok.extern.slf4j.Slf4j;

/**
 * Repository 단위 테스트
 * 
 * @author msyu
 *
 */
@Slf4j
@RunWith(SpringRunner.class)
@MybatisTest
@AutoConfigureTestDatabase(replace= Replace.NONE)
public class SpreadsRepositoryTest {
	
	@Autowired
	private SpreadsRepository spreadsRepository;
	
	@Before
	public void init() {
		t1_receive_spreading_amount(); //받기 테스트를 위해 미리 뿌리기
		t2_receive_spreading_amount_detail(); //받기 테스트를 위해 미리 뿌리기 상세
		t1_info_getReceive(); // 조회 테스트를 위해 다른사용자가 가져가기
	}
	
	@After
	public void cleanup() {
		//spreadsRepository.deleteSpread();
	}
	
	public final static long USER_ID = 999;
	public final static String ROOM_ID = "qie92o";
	public final static long AMOUNT = 10000;
	public final static int PEOPLE = 5;
	public final static String ACCESS_TOKEN = "3e4";
	
	public final static long REV_USER_ID = 888;
	public final static String REV_ROOM_ID = "wi389j";
	public final static long REV_AMOUNT = 10000;
	public final static int REV_PEOPLE = 5;
	public final static String REV_ACCESS_TOKEN = "1q2";
	
	public final static long REV_USER_ID_1 = 777;
	public final static long REV_USER_ID_2 = 666;
	
	/**
	 *  본인이 뿌렸는지 확인
	 */
	@Test
	public void t1_chk_spreading() {
		//given
		long user_id = USER_ID;
		String room_id = ROOM_ID;
		
		//when
		int isExistSpread = spreadsRepository.chkSpread(Spreads.builder().user_id(user_id).room_id(room_id).build());
		//then
		assertThat(isExistSpread).isEqualTo(0);
		log.info("isExist is {}",isExistSpread);
	}
	
	/**
	 * 돈 뿌리기
	 */
	@Transactional
	@Test
	public void t2_spreading_amount() {
		//given
		long user_id = USER_ID;
		String room_id = ROOM_ID;
		long amount = AMOUNT;
		int	people = PEOPLE;
		String access_token = ACCESS_TOKEN;

		spreadsRepository.insertSpread(Spreads.builder().user_id(user_id).room_id(room_id).amount(amount).people(people).access_token(access_token).build());
		//when
		Spreads spread = spreadsRepository.getSpread(Spreads.builder().user_id(user_id).room_id(room_id).amount(amount).people(people).access_token(access_token).build());
		//then
		assertThat(spread.getUser_id()).isEqualTo(user_id);
		assertThat(spread.getRoom_id()).isEqualTo(room_id);
		assertThat(spread.getAmount()).isEqualTo(amount);
		assertThat(spread.getPeople()).isEqualTo(people);
		assertThat(spread.getAccess_token()).isEqualTo(access_token);
	}
	
	/**
	 * 인원에 따른 뿌리기들
	 */
	@Transactional
	@Test
	public void t3_spreading_amount_detail() {
		//given
		String room_id = ROOM_ID;
		long[]  amountBag = {2000,3000,5000};
		String access_token = ACCESS_TOKEN;
		
		//when
		for(long amount : amountBag) {
			spreadsRepository.insertSpreadsPerPerson(SpreadDetail.builder().room_id(room_id).amount(amount).access_token(access_token).build());		
		}
		
		int countspreadDetailByAccesstoken = spreadsRepository.countSpreadDetailByAccesstoken(SpreadDetail.builder().access_token(access_token).build());
		
		//then
		assertThat(countspreadDetailByAccesstoken).isEqualTo(amountBag.length);
		
	}
	
	
	/**
	 *  인증 토큰 발급 테스트
	 */
	@Test
	public void t4_getAccess_token() {
		//when
		String access_token = spreadsRepository.getAccessToken();
		//then
		assertThat(access_token).isNotBlank();
		assertThat(access_token.length()).isEqualTo(3);
		log.info("token is {}",access_token);
		
	}
	
	/**
	 * 인원에 따른 금액 분할 테스트
	 */
	@Test
	public void t5_spreads_AmountByPeople() {
		int people = 5;
		long amount = 1000;
		long[] amountBag = new long[people];
		int count = 0;
		int trans_amount =  Long.valueOf(amount).intValue();
		 
		Random random = new Random();
		for(int i = 0; i < people-1; i++) {
			amountBag[i] = random.nextInt(trans_amount-(people-(i+1)))+1; //최소 금액 1원을 주기 위해 랜덤 금액을 구할 때 남은 인원만큼 랜덤에서 제외하고 1원부터시작하도록 한다.
			trans_amount -= Long.valueOf(amountBag[i]).intValue(); //구한 금액을 뿌린금액에서 제외한다.
		}
		amountBag[people-1] = trans_amount; //마지막 남은 금액을 넣는다.
		
		// 위에까지 진행 할때 문제점은 점점 1에 수렴하기 때문에 랜덤으로 각 금액의 배열을 변경해 준다.
		int rnd = 0;
		long temp = 0;
		for(int j=0; j < people; j++) {
			rnd = random.nextInt(people-j);
			temp = amountBag[j];
			amountBag[j] = amountBag[rnd];
			amountBag[rnd] = temp;
		}
		
		for(long t_amount: amountBag) {
			count++;
			log.info("amount is {}",t_amount);
		}
		
		log.info("create count is {}",count);
		
	}
	
	//받기 API Test
	
	/**
	 * 대화방 번호화 토큰을 이용하여 존재 여부 확인 및
	 * 있을 경우 경과 시간, 누가 뿌렸는지  확인
	 */
	
	//미리 돈 뿌리기 실행
	
	@Transactional
	@Test
	public void t1_receive_spreading_amount() {
		//given
		long user_id = REV_USER_ID;
		String room_id = REV_ROOM_ID;
		long amount = REV_AMOUNT;
		int	people = REV_PEOPLE;
		String access_token = REV_ACCESS_TOKEN;

		spreadsRepository.insertSpread(Spreads.builder().user_id(user_id).room_id(room_id).amount(amount).people(people).access_token(access_token).build());
		//when
		Spreads spread = spreadsRepository.getSpread(Spreads.builder().user_id(user_id).room_id(room_id).amount(amount).people(people).access_token(access_token).build());
		//then
	}
	
	/**
	 * 미리 인원에 따른 뿌리기들
	 */
	@Transactional
	@Test
	public void t2_receive_spreading_amount_detail() {
		//given
		String room_id = REV_ROOM_ID;
		long[]  amountBag = {2000,3000,5000};
		String access_token = REV_ACCESS_TOKEN;
		
		//when
		for(long amount : amountBag) {
			spreadsRepository.insertSpreadsPerPerson(SpreadDetail.builder().room_id(room_id).amount(amount).access_token(access_token).build());		
		}
		
	}
	
	/**
	 * 토큰에 대한 방확인
	 */
	@Test
	public void t1_receive() {
		//given
		String room_id = REV_ROOM_ID;
		String access_token =REV_ACCESS_TOKEN;
		//when
		Spreads spread = spreadsRepository.chkTokenAndRoom(Spreads.builder().room_id(room_id).access_token(access_token).build());
		//then
		assertThat(spread.getTime_diff()).isNotNull();
		log.info("time diff is {} second and user_id is {}",spread.getTime_diff(),spread.getUser_id());
	}
	
	/**
	 * 해당 사용자가 받았는지 확인하며 받았으면 금액을 리턴한다.
	 */
	@Test
	public void t2_receive_yes_or_no() {
		//given
		long user_id = REV_USER_ID;
		String access_token = REV_ACCESS_TOKEN;
		//when
		Spreads spread = spreadsRepository.chkIsReceived(Spreads.builder().user_id(user_id).access_token(access_token).build());
		//then
		assertThat(spread.getAmount()).as("chk receive yes(!0) or no(0)").isEqualTo(0);
		log.info("isReceived is {}", spread.getAmount());
		
	}
	
	/**
	 * 돈 받기
	 */
	@Test
	public void t3_getReceive() {
		//given
		long user_id = REV_USER_ID_1;
		String access_token = REV_ACCESS_TOKEN;
		//when
		int getReceive = spreadsRepository.getReceive(Spreads.builder().user_id(user_id).access_token(access_token).build());
		//then
		assertThat(getReceive).as("get receive yes(1) or no(0)").isEqualTo(1);
	}
	
	//조회 API Test
	
	/**
	 * 돈 받기
	 */
	@Test
	public void t1_info_getReceive() {
		//given
		long user_id = REV_USER_ID_2;
		String access_token = REV_ACCESS_TOKEN;
		//when
		int getReceive = spreadsRepository.getReceive(Spreads.builder().user_id(user_id).access_token(access_token).build());
		//then
		assertThat(getReceive).as("get receive yes(1) or no(0)").isEqualTo(1);
	}
	
	/**
	 * 토큰과 사용자ID를 이용하여 뿌렸는지 확인하고 시간을 가져와서 유효기간을 확인한다.
	 */
	@Test
	public void t2_check() {
		//given
		long user_id = REV_USER_ID;
		String access_token =REV_ACCESS_TOKEN;
		//when
		Spreads spread = spreadsRepository.chkUserIdAndToken(Spreads.builder().user_id(user_id).access_token(access_token).build());
		//then
		assertThat(spread.getUser_id()).isEqualTo(user_id);
	}
	
	/**
	 * 뿌린 사용자와 토큰을 이용하여 현재 상태를 조회한다.
	 */
	@SuppressWarnings("unchecked")
	@Test
	public void t3_getInfo() {
		//given
		long user_id = REV_USER_ID;
		String access_token =REV_ACCESS_TOKEN;
		//when
		Spreads spread = spreadsRepository.getInfoById(Spreads.builder().user_id(user_id).access_token(access_token).build());
		//then
		assertThat(spread.getInput_date()).isNotNull();
		assertThat(spread.getAmount()).isNotNull();
		log.info("input date is {}",spread.getInput_date());
		
		List<RevUserDto> revUserList = new ArrayList<RevUserDto>();
		//받기 완료된 정보(받은금액, 받은 사용자 아이디)
		List<SpreadDetail> list = spreadsRepository.getRevInfo(SpreadDetail.builder().access_token(access_token).build());
		
		// 5. 필요 정보만 Dto에 담기
		list.stream().forEach(revUser -> revUserList.add(new RevUserDto(revUser)));
		
		long tot_rev_amount = 0;
		if(revUserList.size() > 0) {
			tot_rev_amount = revUserList.stream().map(x -> x.getAmount()).reduce((b,c) -> b+c).get();
		}
		log.info("total rev amount is {}",tot_rev_amount);
		
		for(RevUserDto userInfo: revUserList ) {
			log.info("id is {} and amount is {}",userInfo.getUser_id(),userInfo.getAmount());
		}
	}
}
