package com.kakaopay.spread.api.web.dto;

import com.kakaopay.spread.api.domain.spreads.SpreadDetail;
import com.kakaopay.spread.api.domain.spreads.Spreads;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 *  뿌리기 요청값
 * @author msyu
 *
 */
@Getter
@NoArgsConstructor
public class SpreadsRequestDto {
	private long user_id; //사용자 아이디
	private String room_id; //소속  대화방
	private long amount; // 금액
	private int people; //인원
	private String access_token;
	
	@Builder
	public SpreadsRequestDto(long user_id, String room_id, long amount, int people, String access_token) {
		this.user_id = user_id;
		this.room_id = room_id;
		this.amount = amount;
		this.people = people;
		this.access_token = access_token;
	}
	
	/*
	 * @Builder public SpreadsRequestDto(long user_id, String room_id, long amount,
	 * int people) { this.user_id = user_id; this.room_id = room_id; this.amount =
	 * amount; this.people = people; }
	 * 
	 * @Builder public SpreadsRequestDto(long amount, int people) { this.amount =
	 * amount; this.people = people; }
	 */
	
	public Spreads toEntity() {
		return Spreads.builder()
					.user_id(user_id)
					.room_id(room_id)
					.amount(amount)
					.people(people)
					.access_token(access_token)
					.build();
	}
	
	public SpreadDetail toDetailEntity() {
		return SpreadDetail.builder()
						.room_id(room_id)
						.amount(amount)
						.access_token(access_token)
						.build();
	}
	
	
}
