package com.kakaopay.spread.api.web.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 받기 응답값
 * @author msyu
 *
 */
@Getter
@NoArgsConstructor
public class ReceiveResponseDto {
	private long amount;
	private String rtnCode;
	
	@Builder
	public ReceiveResponseDto(long amount, String rtnCode) {
		this.amount = amount;
		this.rtnCode = rtnCode;
	}

}
