package com.kakaopay.spread.api.web.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 뿌리기 응답값
 * @author msyu
 *
 */
@Getter
@NoArgsConstructor
public class SpreadsResponseDto {
	private String access_token;
	private String rtnCode;
	
	@Builder
	public SpreadsResponseDto(String access_token, String rtnCode) {
		this.access_token = access_token;
		this.rtnCode = rtnCode;
	}
	

}
