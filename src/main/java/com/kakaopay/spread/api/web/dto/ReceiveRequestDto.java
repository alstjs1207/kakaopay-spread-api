package com.kakaopay.spread.api.web.dto;

import com.kakaopay.spread.api.domain.spreads.Spreads;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 *  받기 요청값
 * @author msyu
 *
 */
@Getter
@NoArgsConstructor
public class ReceiveRequestDto {
	private long user_id; //사용자 아이디
	private String room_id; //소속  대화방
	private String access_token; //토큰
	private long amount; //받은 돈
	private int time_diff; //시간 차이
	
	@Builder
	public ReceiveRequestDto(long user_id, String room_id, String access_token, long amount, int time_diff) {
		this.user_id = user_id;
		this.room_id = room_id;
		this.access_token = access_token;
		this.amount = amount;
		this.time_diff = time_diff;
	}
	
	public Spreads toEntity() {
		return Spreads.builder()
				.user_id(user_id)
				.room_id(room_id)
				.access_token(access_token)
				.build();
	}
	
}
