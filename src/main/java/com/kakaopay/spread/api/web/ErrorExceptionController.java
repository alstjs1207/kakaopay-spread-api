package com.kakaopay.spread.api.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.kakaopay.spread.api.common.ErrorCode;
import com.kakaopay.spread.api.common.ErrorEntity;
import com.kakaopay.spread.api.common.ErrorException;

import lombok.extern.slf4j.Slf4j;

/**
 * Error Controller
 * @author msyu
 *
 */
@Slf4j
@Controller
@ControllerAdvice
public class ErrorExceptionController {
	
	@ExceptionHandler(ErrorException.class)
	protected ResponseEntity<ErrorEntity> handle(ErrorException ee) {
		log.error("ErrorException - ",ee);
		
		ErrorCode errorCodeE = ee.getErrorCode();
		
		return new ResponseEntity<>(ErrorEntity.builder()
											.rtnCode(errorCodeE.getRtnCode()) //error에 대한 응답 코드
											.rtnMsg(errorCodeE.getRtnMsg()) // error에 대한 응답 메시지
											.build(), 
									HttpStatus.resolve(errorCodeE.getStatus()));
	}

}
