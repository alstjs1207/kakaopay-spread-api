package com.kakaopay.spread.api.web;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.kakaopay.spread.api.service.info.InfoService;
import com.kakaopay.spread.api.web.dto.InfoRequestDto;
import com.kakaopay.spread.api.web.dto.InfoResponseDto;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 뿌리기 API
 * 
 * @author msyu
 *
 */
@Slf4j
@RequiredArgsConstructor
@RestController
public class InfoApiController {
	
	private final InfoService InfoService;
	
	@PostMapping("/kpay/restv1/info")
	public InfoResponseDto info(@RequestHeader(value = "X-USER-ID") String user_id,
						 @RequestHeader(value = "X-ROOM-ID") String room_id,
						 @Validated @RequestBody InfoRequestDto requestDto) {
		
		log.info("access_toekn is {}",requestDto.getAccess_token());
		
		requestDto = InfoRequestDto.builder()
								.access_token(requestDto.getAccess_token())
								.user_id(Long.parseLong(user_id))
								.room_id(room_id)
								.build();
		
		return InfoService.getInfo(requestDto);
	}

}
