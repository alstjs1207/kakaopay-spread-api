package com.kakaopay.spread.api.web.dto;

import java.util.List;

import com.kakaopay.spread.api.domain.spreads.SpreadDetail;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 조회 응답값
 * @author msyu
 *
 */
@Getter
@NoArgsConstructor
public class InfoResponseDto {
	private String input_date; //뿌린 시각
	private long tot_rev_amount; // 받기 완료된 금액
	private long amount; //금액
	private List<RevUserDto> revUserList;
	private String rtnCode;
	
	@Builder
	public InfoResponseDto(String input_date, long tot_rev_amount,
							long amount, String room_id, String rtnCode, List<RevUserDto> revUserList) {
		this.input_date = input_date;
		this.tot_rev_amount = tot_rev_amount;
		this.amount = amount;
		this.rtnCode = rtnCode;
		this.revUserList = revUserList;
	}
	

}
