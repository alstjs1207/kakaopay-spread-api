package com.kakaopay.spread.api.web;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.kakaopay.spread.api.common.ApiCommonsDef;
import com.kakaopay.spread.api.service.spreads.SpreadsService;
import com.kakaopay.spread.api.web.dto.SpreadsRequestDto;
import com.kakaopay.spread.api.web.dto.SpreadsResponseDto;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 뿌리기 API
 * 
 * @author msyu
 *
 */
@Slf4j
@RequiredArgsConstructor
@RestController
public class SpreadApiController {
	
	
	private final SpreadsService spreadsService;
	
	@PostMapping("/kpay/restv1/spread")
	public SpreadsResponseDto spread(@RequestHeader(value = "X-USER-ID") String user_id,
						 @RequestHeader(value = "X-ROOM-ID") String room_id,
						 @Validated @RequestBody SpreadsRequestDto requestDto) {
		
		log.info("request Dto - amount : {}, people : {}",requestDto.getAmount(),requestDto.getPeople());
		
		requestDto = SpreadsRequestDto.builder()
									.user_id(Long.parseLong(user_id))
									.room_id(room_id)
									.amount(requestDto.getAmount())
									.people(requestDto.getPeople())
									.access_token(spreadsService.getAccessToken())
									.build();
		//뿌리기 서비스
		String access_token = spreadsService.spreading_amount(requestDto);
		
		log.info("access_token is {}",access_token);
		
		return SpreadsResponseDto.builder()
				.access_token(access_token)
				.rtnCode(ApiCommonsDef.SUCCESS)
				.build();
	}

}
