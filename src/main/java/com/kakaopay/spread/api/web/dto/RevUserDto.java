package com.kakaopay.spread.api.web.dto;

import com.kakaopay.spread.api.domain.spreads.SpreadDetail;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 받기 완료한 사용자 정보
 * @author msyu
 *
 */
@Getter
@NoArgsConstructor
public class RevUserDto {
	private long user_id;
	private long amount;

	public RevUserDto(SpreadDetail user) {
		this.user_id = user.getUser_id();
		this.amount = user.getAmount();
	}
}
