package com.kakaopay.spread.api.web;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.kakaopay.spread.api.common.ApiCommonsDef;
import com.kakaopay.spread.api.service.recevie.ReceiveService;
import com.kakaopay.spread.api.web.dto.ReceiveRequestDto;
import com.kakaopay.spread.api.web.dto.ReceiveResponseDto;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 뿌리기 API
 * 
 * @author msyu
 *
 */
@Slf4j
@RequiredArgsConstructor
@RestController
public class ReceiveApiController {
	
	private final ReceiveService receiveService;
	
	@PutMapping("/kpay/restv1/receive")
	public ReceiveResponseDto spread(@RequestHeader(value = "X-USER-ID") String user_id,
						 @RequestHeader(value = "X-ROOM-ID") String room_id,
						 @Validated @RequestBody ReceiveRequestDto requestDto) {
		
		
		log.info("access_token : {}",requestDto.getAccess_token());
		
		requestDto = ReceiveRequestDto.builder()
									.user_id(Long.parseLong(user_id))
									.room_id(room_id)
									.access_token(requestDto.getAccess_token())
									.build();
		
		long amount = receiveService.recevie_amount(requestDto);
		
		log.info("get amount is {}",amount);
		
		return ReceiveResponseDto.builder()
								.amount(amount)
								.rtnCode(ApiCommonsDef.SUCCESS)
								.build();
	}
				

}
