package com.kakaopay.spread.api.domain.spreads;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 뿌리기 Entity
 * 
 * @author msyu
 *
 */

@ToString
@Getter
@NoArgsConstructor
public class Spreads {
	private String input_date; //뿌린 시각
	private long user_id; //사용자 아이디
	private String room_id; //소속  대화방
	private long amount; // 금액
	private int people; //인원
	private String access_token; //인증 토큰
	private int time_diff; //시간 차이
	
	@Builder
	public Spreads(String input_date, long user_id, String room_id, long amount, int people, String access_token, int time_diff) {
		this.input_date = input_date;
		this.user_id = user_id;
		this.room_id = room_id;
		this.amount = amount;
		this.people = people;
		this.access_token = access_token;
		this.time_diff = time_diff;
	}
	
}
