package com.kakaopay.spread.api.domain.spreads;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 뿌리기 상세 Entity
 * 
 * @author msyu
 *
 */

@ToString
@Getter
@NoArgsConstructor
public class SpreadDetail {
	private String output_date; //가져간 시간 
	private long user_id; //사용자 아이디
	private String room_id; //소속  대화방
	private long amount; // 금액
	private int people; //인원
	private String access_token; //인증 토큰
	private int seq; //순서
	
	@Builder
	public SpreadDetail(String output_date, long user_id, String room_id, long amount, int people, String access_token, int seq) {
		this.output_date = output_date;
		this.user_id = user_id;
		this.room_id = room_id;
		this.amount = amount;
		this.people = people;
		this.access_token = access_token;
		this.seq = seq;
	}
}
