package com.kakaopay.spread.api.domain.spreads;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.kakaopay.spread.api.web.dto.InfoResponseDto;

/**
 * 뿌리기 API repository
 * @author msyu
 *
 */
@Repository
@Mapper
public interface SpreadsRepository {
	
	//Spreads
	// access_token 생성
	String getAccessToken();
	
	// 뿌리기
	void insertSpread(Spreads spread);
	
	// 인원에 맞는 뿌리기 금액 분할
	void insertSpreadsPerPerson(SpreadDetail spreadDetail);
	
	// 뿌리기 정보
	Spreads getSpread(Spreads spread);
	
	// 뿌렸는지 체크
	int chkSpread(Spreads spread);
	
	//Receive
	//토큰과 대화방 아이디를 이용하여 유효한 시간 체크
	Spreads chkTokenAndRoom(Spreads spread);
	
	//인증된 사용자 중 돈을 이미 받았는지 체크
	Spreads chkIsReceived(Spreads spread);
	
	// 안받았으면 뿌린 돈 선점
	int getReceive(Spreads spread);
	
	//Info
	// 자신이 뿌린 정보를 찾는다.
	Spreads chkUserIdAndToken(Spreads spread);
	
	// 뿌린 사용자의 정보를 가져온다.
	Spreads getInfoById(Spreads spread);
	
	// 뿌린돈을 가져간 사용자의 목록을 가져온다.
	List getRevInfo(SpreadDetail spreadDetail);
	
	
	//Test 용
	int countSpreadDetailByAccesstoken(SpreadDetail spreadDetail);
	void deleteSpread();

}
