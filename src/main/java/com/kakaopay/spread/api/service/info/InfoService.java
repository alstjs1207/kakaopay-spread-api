package com.kakaopay.spread.api.service.info;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kakaopay.spread.api.common.ApiCommonsDef;
import com.kakaopay.spread.api.common.ErrorCode;
import com.kakaopay.spread.api.common.ErrorException;
import com.kakaopay.spread.api.domain.spreads.SpreadDetail;
import com.kakaopay.spread.api.domain.spreads.Spreads;
import com.kakaopay.spread.api.domain.spreads.SpreadsRepository;
import com.kakaopay.spread.api.web.dto.InfoRequestDto;
import com.kakaopay.spread.api.web.dto.InfoResponseDto;
import com.kakaopay.spread.api.web.dto.RevUserDto;

import lombok.RequiredArgsConstructor;

@Transactional
@RequiredArgsConstructor
@Service
public class InfoService {
	
	
	private final SpreadsRepository spreadsRepository;
	
	@SuppressWarnings("unchecked")
	public InfoResponseDto getInfo(InfoRequestDto requestDto) {
		
		long tot_rev_amount = 0;
		
		// 1. 해당 토큰이 뿌린사용자 것이 아니거나 유효하지 않는 토큰인 경우 에러
		Spreads spread = Optional.ofNullable(spreadsRepository.chkUserIdAndToken(requestDto.toEntity()))
				.orElseThrow(() -> new ErrorException(ErrorCode.INVALID_TOKEN));
		
		// 2. 유효기간 7일이 지난경우 에러
		if(spread.getTime_diff() > ApiCommonsDef.SPREAD_INFO_TIMEOUT) {
			throw new ErrorException(ErrorCode.NOT_SEARCH_TIME);
		}
		
		// 3. 뿌린 사용자에 대한 시각, 금액 정보 조회
		spread = spreadsRepository.getInfoById(requestDto.toEntity());
		
		List<RevUserDto> revUserList = new ArrayList<RevUserDto>();
		
		// 4. 받기 완료된 사용자 리스트 조회
		List<SpreadDetail> list = spreadsRepository.getRevInfo(requestDto.toDetailEntity());
		
		// 5. 필요 정보만 Dto에 담기
		list.stream().forEach(revUser -> revUserList.add(new RevUserDto(revUser)));
		
		// 6. 받기 완료된 금액
		if(revUserList.size() > 0) {
			tot_rev_amount = revUserList.stream().map(x -> x.getAmount()).reduce((b,c) -> b+c).get();
		}
		
		return InfoResponseDto.builder()
							.input_date(spread.getInput_date()) //뿌린 시각
							.amount(spread.getAmount()) //뿌린 금액
							.tot_rev_amount(tot_rev_amount) // 받기 완료된 금액
							.revUserList(revUserList) // 받기 완료된 정보
							.rtnCode(ApiCommonsDef.SUCCESS) //응답 코드
							.build();
		
	}
	

}
