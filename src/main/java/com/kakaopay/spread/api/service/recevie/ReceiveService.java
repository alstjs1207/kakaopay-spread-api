package com.kakaopay.spread.api.service.recevie;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kakaopay.spread.api.common.ApiCommonsDef;
import com.kakaopay.spread.api.common.ErrorCode;
import com.kakaopay.spread.api.common.ErrorException;
import com.kakaopay.spread.api.domain.spreads.Spreads;
import com.kakaopay.spread.api.domain.spreads.SpreadsRepository;
import com.kakaopay.spread.api.web.dto.ReceiveRequestDto;

import lombok.RequiredArgsConstructor;

/**
 *  받기 service
 *  
 * @author msyu
 *
 */
@RequiredArgsConstructor
@Service
public class ReceiveService {
	
	
	private final SpreadsRepository spreadsRepository;
	
	@Transactional
	public long recevie_amount(ReceiveRequestDto requestDto) {
		
		long getAmount = 0;
		
		// 1. 해당 인증 토큰에 대한 방이 없는 경우 에러
		Spreads spread = Optional.ofNullable(spreadsRepository.chkTokenAndRoom(requestDto.toEntity()))
				.orElseThrow(() -> new ErrorException(ErrorCode.INVALID_TOKEN));
		
		// 2. 인증 토큰이 만료된 경우 에러
		if(spread.getTime_diff() > ApiCommonsDef.ACCESS_TOKEN_TIMEOUT) {
			throw new ErrorException(ErrorCode.EXPIRED_TOKEN);
		}
		
		// 3. 뿌린 사용자와 동일 사용자인 경우 받을 수 없기 때문에 에러
		if(spread.getUser_id() == requestDto.getUser_id()) {
			throw new ErrorException(ErrorCode.ALREADY_SPREAD_NO_RECEIVE);
		}
		
		// 4. 해당 사용자가 받을 수 있는지 확인
		spread = spreadsRepository.chkIsReceived(requestDto.toEntity());
		
		// 4-1. 이미 돈을 받은 경우 에러
		if(spread.getAmount() != 0) {
			throw new ErrorException(ErrorCode.ALREADY_GET_RECEIVE);
		}
		
		// 5. 돈 받기
		int resultGetReceive = spreadsRepository.getReceive(requestDto.toEntity());
		
		// 5-1. 줄 수 있는 돈이 없는 경우 에러
		if(resultGetReceive == 0) {
			throw new ErrorException(ErrorCode.NO_RECEIVE);
		}
		// 5-2. 받은 돈 가져오기
		else {
			spread = spreadsRepository.chkIsReceived(requestDto.toEntity());
			getAmount = spread.getAmount();
			
		}
			
		return getAmount;
	}
}
