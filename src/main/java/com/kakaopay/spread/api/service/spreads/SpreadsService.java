package com.kakaopay.spread.api.service.spreads;

import java.util.Random;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kakaopay.spread.api.common.ApiCommonsDef;
import com.kakaopay.spread.api.common.ErrorCode;
import com.kakaopay.spread.api.common.ErrorException;
import com.kakaopay.spread.api.domain.spreads.SpreadDetail;
import com.kakaopay.spread.api.domain.spreads.SpreadsRepository;
import com.kakaopay.spread.api.web.dto.SpreadsRequestDto;

import lombok.RequiredArgsConstructor;

/**
 *  뿌리기 service
 *  
 * @author msyu
 *
 */
@RequiredArgsConstructor
@Service
public class SpreadsService {
	
	
	private final SpreadsRepository spreadRepository;
	
	public String getAccessToken() {
		return spreadRepository.getAccessToken();
	}
	
	/**
	 *  뿌리기
	 *  
	 * @param requestDto
	 * @return access_token
	 */
	@Transactional
	public String spreading_amount(SpreadsRequestDto requestDto) {
		
		// 1. 금액보다 인원이 많으면 에러
		if(requestDto.getPeople() > requestDto.getAmount()) {
			throw new ErrorException(ErrorCode.MANY_PEOPLE);
		}
		
		//2. 뿌렸는지 확인하여 이미 뿌렸으면 에러 ...변경 동일한 방에 동일한 사용자가 다시 뿌려도 안된다는 제약이 없어보임
//		if( ApiCommonsDef.DEFAULT_COUNT_ZERO < spreadRepository.chkSpread(requestDto.toEntity())) {
//			throw new ErrorException(ErrorCode.ALREADY_SPREAD);
//		}
		//2. 인원없이 돈을 뿌리면 에러
		if(requestDto.getPeople() == 0) {
			throw new ErrorException(ErrorCode.NO_PEOPLE);
		}
		
		//뿌리기
		spreadRepository.insertSpread(requestDto.toEntity());
		
		//인원에 맞게 금액 나누기
		long[] amountBag = mkAmountPerPerson(requestDto.getAmount(),requestDto.getPeople());
		//금액 배열 랜덤 변경
		amountBag = mkRandomDivision(amountBag, requestDto.getPeople());
		
		//인원에 따른 뿌리기
		for(long amount: amountBag ) {
			
			spreadRepository.insertSpreadsPerPerson(SpreadDetail.builder()
																.room_id(requestDto.getRoom_id())
																.access_token(requestDto.getAccess_token())
																.amount(amount)
																.build());
		
		}
		
		//토큰 리턴
		return requestDto.getAccess_token();
		
	}
	
	/**
	 * 뿌린 금액을 인원에 맞게 랜덤하게 분할한다. 
	 * @param amount
	 * @param people
	 * @return 뿌린 금액을 나눈 돈 주머니
	 */
	public long[] mkAmountPerPerson(long amount, int people) {
		long[] amountBag = new long[people];
		
		int trans_amount =  Long.valueOf(amount).intValue();
		
		Random random = new Random();
		for(int i = 0; i < people-1; i++) {
			amountBag[i] = random.nextInt(trans_amount-(people-(i+1)))+1; //최소 금액 1원을 주기 위해 랜덤 금액을 구할 때 남은 인원만큼 랜덤에서 제외하고 1원부터시작하도록 한다.
			trans_amount -= Long.valueOf(amountBag[i]).intValue(); //구한 금액을 뿌린금액에서 제외한다.
		}
		amountBag[people-1] = trans_amount; //마지막 남은 금액을 넣는다.
		
		return amountBag;
	}
	
	/**
	 * 위에까지 진행 할때 문제점은 점점 1에 수렴하기 때문에 랜덤으로 각 금액의 배열을 변경해 준다.
	 * @param amountBag
	 * @param people
	 * @return 뿌린 금액을 랜덤하게 변경한 돈 주머니
	 */
	public long[] mkRandomDivision(long[] amountBag, int people) {
		int rnd = 0;
		long temp = 0;
		Random random = new Random();
		for(int j=0; j < people; j++) {
			rnd = random.nextInt(people-j);
			temp = amountBag[j];
			amountBag[j] = amountBag[rnd];
			amountBag[rnd] = temp;
		}
		
		return amountBag;
	}
	

}
