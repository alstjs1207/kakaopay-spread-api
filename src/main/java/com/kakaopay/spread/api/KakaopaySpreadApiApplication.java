package com.kakaopay.spread.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KakaopaySpreadApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KakaopaySpreadApiApplication.class, args);
	}

}
