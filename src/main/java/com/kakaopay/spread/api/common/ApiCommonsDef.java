package com.kakaopay.spread.api.common;

/**
 * API code 정의
 * @author msyu
 *
 */
public class ApiCommonsDef {
	
	public final static String SUCCESS = "KP_SUCCESS"; //성공 응답 코드
	public final static int DEFAULT_COUNT_ZERO = 0;
	public final static int ACCESS_TOKEN_TIMEOUT = 600; //10분 인증 토큰 만료
	public final static int SPREAD_INFO_TIMEOUT = 604800; //7일 조회 만료

}
