package com.kakaopay.spread.api.common;

import lombok.Getter;
/**
 *  errorcode 정리
 *  
 * @author msyu
 *
 */
@Getter
public enum ErrorCode {
	
	INVALID_TOKEN(400,"KP_ER001","유효한 인증 토큰이 아닙니다."),
	EXPIRED_TOKEN(400,"KP_ER002","인증 토큰이 만료되었습니다."),
	NOT_SEARCH_TIME(400,"KP_ER003","조회 기간이 만료되었습니다."),
	MANY_PEOPLE(400,"KP_ER004","금액보다 인원이 많습니다."),
	ALREADY_SPREAD(400,"KP_ER005","이미 해당 대화방에 돈을 뿌렸습니다."),
	ALREADY_SPREAD_NO_RECEIVE(400,"KP_ER006","뿌린 사람은 해당 대화방에서 돈을 받지 못합니다."),
	ALREADY_GET_RECEIVE(400,"KP_ER007","해당 대화방에서 이미 돈을 받았습니다."),
	NO_RECEIVE(400,"KP_ER008","받을 수 있는 돈이 없습니다."),
	NO_PEOPLE(400,"KP_ER009","인원없이 돈을 뿌릴 수는 없습니다."),
	ERROR(400,"KP_ER999","알수없는 에러가 발생했습니다.");
	
	private final String rtnCode; //응답코드
	private final String rtnMsg; //응답 메시지
	private final int status; // http 상태
	
	
	ErrorCode(final int status, final String rtnCode, final String rtnMsg) {
		this.rtnCode = rtnCode;
		this.rtnMsg = rtnMsg;
		this.status = status;
	}
}
