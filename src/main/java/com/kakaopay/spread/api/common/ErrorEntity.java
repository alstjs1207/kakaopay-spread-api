package com.kakaopay.spread.api.common;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
/**
 *  Error Entity
 * @author msyu
 *
 */
@Getter
@NoArgsConstructor
public class ErrorEntity {
	
	private String rtnCode; //응답 코드
	private String rtnMsg; //응답 메시지

	@Builder
	ErrorEntity(String rtnCode, String rtnMsg) {
		this.rtnCode = rtnCode;
		this.rtnMsg = rtnMsg;
	}
	
}
