package com.kakaopay.spread.api.common;

/**
 * error Exception
 * @author msyu
 *
 */
public class ErrorException  extends RuntimeException{
	private static final long serialVersionUID = 1L;
	
	private ErrorCode errorCodeE;
	
	public ErrorException(ErrorCode errorCodeE) {
		super(errorCodeE.getRtnMsg());
		this.errorCodeE = errorCodeE;
	}
	
	public ErrorCode getErrorCode() {
		return errorCodeE;
	}
	
}
