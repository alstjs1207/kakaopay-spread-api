DROP TABLE IF EXISTS spread_info;
create table spread_info
(
	seq          bigint       not null auto_increment primary key,
	input_date   datetime     not null,
    access_token char(3)      not null,
    user_id      bigint       not null,
	room_id      varchar(200) not null,
    amount       bigint       not null,
    people		   bigint       not null,
	unique key access_token_unique (access_token)
);

DROP TABLE IF EXISTS spread_info_detail;
create table spread_info_detail
(
	seq        		  int      not null auto_increment primary key,
  	output_date  		datetime null DEFAULT NULL,
	access_token    char(3)  not null,
	user_id    		  bigint   null,
	room_id      	  varchar(200) not null,
  	amount     		  bigint   not null,
	unique key seq_unique (seq),
  constraint fk_spread_info_detail foreign key (access_token) references spread_info(access_token) ON DELETE CASCADE
);