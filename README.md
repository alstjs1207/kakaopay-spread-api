# 카카오페이 뿌리기 기능 구현하기

## 요구사항
- 뿌리기, 받기, 조회 기능을 수행하는 REST API 를 구현합니다.
    - 요청한 사용자의 식별값은 숫자 형태이며 `X-USER-ID` 라는 HTTP Header로 전달됩니다.
    - 요청한 사용자가 속한 대화방의 식별값은 문자 형태이며 `X-ROOM-ID` 라는 HTTP Header로 전달됩니다.
    - 모든 사용자는 뿌리기에 충분한 잔액을 보유하고 있다고 가정하여 별도로 잔액에 관련된 체크는 하지 않습니다.
- 작성하신 어플리케이션이 다수의 서버에 다수의 인스턴스로 동작하더라도 기능에 문제가 없도록 설계되어야 합니다.
- 각 기능 및 제약사항에 대한 단위테스트를 반드시 작성합니다.

## API 리턴 코드 목록
|코드       |코드 내용|
|-----------|----------|
|KP_SUCCESS |성공      |
|KP_ER001   |유효한 인증 토큰이 아닙니다.|
|KP_ER002   |인증 토큰이 만료되었습니다.|
|KP_ER003   |조회 기간이 만료되었습니다.|
|KP_ER004   |금액보다 인원이 많습니다.|
|KP_ER005   |이미 해당 대화방에 돈을 뿌렸습니다.|
|KP_ER006   |뿌린 사람은 해당 대화방에서 돈을 받지 못합니다.|
|KP_ER007   |해당 대화방에서 이미 돈을 받았습니다.|
|KP_ER008   |받을 수 있는 돈이 없습니다.|
|KP_ER009   |인원없이 돈을 뿌릴 수는 없습니다.|


- Example

```
{
    "rtnCode": "KP_ER001"
    "rtnMsg": "유효한 인증 토큰이 아닙니다."
}
```

## 데이터베이스

`MariaDB` 사용하였음.

- Schema

```
create database kTest default character set utf8;
grant all privileges on kTest.* to kpay@'localhost' identified by 'qwer1234';
grant all privileges on kTest.* to kpay@'%' identified by 'qwer1234';
flush privileges;

DROP TABLE IF EXISTS spread_info;
create table spread_info
(
	seq          bigint       not null auto_increment primary key,
	input_date   datetime     not null,
    access_token char(3)      not null,
    user_id      bigint       not null,
	room_id      varchar(200) not null,
    amount       bigint       not null,
    people		   bigint       not null,
	unique key access_token_unique (access_token)
);

DROP TABLE IF EXISTS spread_info_detail;
create table spread_info_detail
(
	seq        		  int      not null auto_increment primary key,
  	output_date  		datetime null DEFAULT NULL,
	access_token    char(3)  not null,
	user_id    		  bigint   null,
	room_id      	  varchar(200) not null,
  	amount     		  bigint   not null,
	unique key seq_unique (seq),
  constraint fk_spread_info_detail foreign key (access_token) references spread_info(access_token) ON DELETE CASCADE
);

```


## 인터페이스 레이아웃

1. 뿌리기 API
- path `POST` `/kpay/restv1/spread`

- Header

|Parameter  |Data Type |contents   |
|-----------|----------|-----------|
|X-USER-ID  |long     |요청 사용자|
|X-ROOM-ID  |String   |소속 대화방|
|Content-Type|String|application/json|

- Parameters

|Parameter|Required|Description|Data Type|
|---------|--------|---------- |---------|
|amount   |true    |금액       | long    |
|people   |true    |인원       | int    |

- Response

|Parameter|Required|Description|Data Type|
|---------|--------|---------- |---------|
|access_token|true    |인증토큰 | String |
|rtnCode   |true    |리턴 코드  | String |
|rtnMsg   |true    |실패 메시지  | String |


- Example

```
{
    "access_token": "06f",
    "rtnCode": "KP_SUCCESS"
}
```


2. 받기 API
- path `PUT` `/kpay/restv1/receive`

- Header

|Parameter  |Data Type |contents   |
|-----------|----------|-----------|
|X-USER-ID  |long     |요청 사용자|
|X-ROOM-ID  |String   |소속 대화방|
|Content-Type|String|application/json|

- Parameters

|Parameter    |Required|Description|Data Type|
|-------------|--------|---------- |---------|
|access_token |true    |인증 토큰   | String  |

- Response

|Parameter|Required|Description|Data Type|
|---------|--------|---------- |---------|
|amount    |true    |금액       | String |
|rtnCode   |true    |리턴 코드  | String |
|rtnMsg   |true    |실패 메시지  | String |


- Example

```
{
    "amount": 385,
    "rtnCode": "KP_SUCCESS"
}
```

3. 조회 API
- path `POST` `/kpay/restv1/info`

- Header

|Parameter  |Data Type |contents   |
|-----------|----------|-----------|
|X-USER-ID  |long     |요청 사용자|
|X-ROOM-ID  |String   |소속 대화방|
|Content-Type|String|application/json|

- Parameters

|Parameter    |Required|Description|Data Type|
|-------------|--------|---------- |---------|
|access_token |true    |인증 토큰   | String  |

- Response

|Parameter     |Required|Description|Data Type|
|--------------|--------|--------------- |---------|
|input_date    |true    |뿌린 시각       | String |
|amount        |true    |뿌린 금액       | long   |
|tot_rev_amount|true    |받기 완료된 금액| long   |
|rtnCode       |true    |리턴 코드       | String |
|rtnMsg        |false   |실패 메시지     | String |
| 리스트 revUserList                              |
|amount        |true    |받은 금액         | long |
|user_id       |true    |받은 사용자 아이디| long|


- Example

```
{
    "input_date": "2020-11-22 14:41:19",
    "tot_rev_amount": 385,
    "amount": 1000,
    "revUserList": [
        {
            "user_id": 891207,
            "amount": 385
        }
    ],
    "rtnCode": "KP_SUCCESS"
}
```


## 핵심 문제해결 전략

> 기존 뿌리기 기능을 보면 `금액보다 인원이 클 수는 없음`을 확인하였습니다.
>  
>  따라서 `최소 1원`을 줘야한다고 생각했습니다.
>  뿌리기 분배 시 전체 금액에서 현재 받는 사용자를 제외한 나머지 인원 수만큼 뺀 후 1원 ~ 해당 금액 만큼 랜덤을 적용하였습니다.
>
>  위에 처럼 할경우 0원이 없는 문제는 없어지지만, 점점 1에 수렴해 집니다.
>
>  따라서 분배를 한 후에 각 금액 배열을 다시 랜덤하게 위치를 변경하여 다양하게 꺼낼 수 있도록 하였습니다.

> 데이터베이스와 직접 관련있는 entity 인 Spreads(뿌리기 정보), SpreadDetail(뿌린 정보)에 대해서는 `직접 변경을 하지 못하도록` 설계하였습니다.
>
> 대신 `요청(Request)`과 `응답(Response)`에 대한 Dto를 따로 만들어서 변경에 대한 부드러운 처리가 이뤄지도록 하였습니다.


## 사전 참고 사항

서버에 해당 사항을 미리 설치 필요

`openjdk-11` 사용

apt-get install -y openjdk-11-jdk

`maven`  `git` 사용

apt-get install -y maven
apt-get install git -y


## 실행 및 배포하기

```
디렉토리 이동
cd /home/pay/step1

git clone https://gitlab.com/alstjs1207/kakaopay-spread-api.git

cd kakaopay-spread-api/

chmod +x ./scripts/deploy.sh

./scripts/deploy.sh //실행

```
